import datetime as dt
def get_prev_work_day(org_date,holidays_list):
    while (org_date  in holidays_list) or (org_date.weekday() in [5, 6]):
        org_date = org_date + dt.timedelta(days=-1)
    return org_date