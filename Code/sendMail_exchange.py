def exg_send_mail(mail_addr, cc_addr, subject, htmlBody, attchmentList,auth="Robot"):
    from exchangelib import DELEGATE, Account, Credentials, Message, Mailbox, HTMLBody, Configuration, NTLM, \
        FileAttachment
    from exchangelib.protocol import BaseProtocol, NoVerifyHTTPAdapter
    import urllib3
    import datetime

    BaseProtocol.HTTP_ADAPTER_CLS = NoVerifyHTTPAdapter
    urllib3.disable_warnings()

    cred = Credentials(r'username', 'password')

    config = Configuration(server='exgmail.cn.test.jp', credentials=cred, auth_type=NTLM)
    a = Account(
        primary_smtp_address="robot@tmp.cn.test.jp", config=config, autodiscover=False, access_type=DELEGATE
    )

    commen_message = '''<div align="left" style="text-align: justify;"><font color="red" face="Arial" size="2" style="font-family: Arial;"><span style="font-size: 10pt;">这是一封系统自动生成的邮件，请勿直接回复！</span></font><div>
                        <div align="left" style="text-align: justify;"><font color="red" face="Arial" size="2" style="font-family: Arial;"><span style="font-size: 10pt;"><u>This mail is from:%s</u></span></font><div>
                        <br><br>
                        '''%auth

    to_list = []
    for x in mail_addr.split(';'):
        if x:
            to_list.append(Mailbox(email_address=x))

    cc_list = []
    for x in cc_addr.split(';'):
        if x:
            cc_list.append(Mailbox(email_address=x))


    m = Message(
        account=a,
        folder=a.sent,
        subject=subject,
        body=HTMLBody(commen_message+htmlBody),
        to_recipients=to_list,
        cc_recipients=cc_list
    )


    for file in attchmentList:
        print(file.split('/')[-1])
        with open(file, 'rb') as f:
            content = f.read()
        new_attach = FileAttachment(name=file.split('/')[-1], content=content)
        m.attach(new_attach)

    m.send_and_save()
