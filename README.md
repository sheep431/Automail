# Purpose
This example can help you to send auto mail through outlook easily

# Way to use the example
1. Just get your own outlook server address, replace it in sendMail_exchange.py function, replace credential info too.
2. Add mail task in main.py with the provided templates
3. Set running schedule in schedule_run.py
