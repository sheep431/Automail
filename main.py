﻿def send_shibor_mail():
    from Code.sendMail_exchange import exg_send_mail
    exg_send_mail("address1","address2;address3","title1","Just confirm today's Shibor submission is completed!","","wk@cn.test.jp")

def send_CLO_mail():
    from Code.sendMail_exchange import exg_send_mail
    exg_send_mail("xyy@cn.test.jp","wk@cn.test.jp","★Morning call from CLO (Please Click and Check)",'''
    <div>
<div class="BodyFragment"><font face="Times New Roman" size="2" style="font-family: Times New Roman,serif,'EmojiFont';"><span style="font-size: 10.5pt;">
<div align="left" style="text-align: justify;"><font color="red"><span style="background-color: yellow;">Let’s think about compliance one more time before your daily work! (check the below points)</span></font></div>
<div align="left" style="text-align: justify;"><font color="red"><span style="background-color: yellow;">If you are in trouble or find something breached the rule, please discuss with your supervisor or me without hesitation.</span></font></div>
<div align="left" style="text-align: justify;">&nbsp;</div>
<div align="left" style="text-align: justify;">Link:</div>
<div align="left" style="text-align: justify;"><a href="fileaddress.xlsx"><font color="blue"><u>folder_address</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>name</u></font><font color="blue" face="MS Mincho" style="font-family: MS Mincho,serif,'EmojiFont';"><u>・</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>コンプラ対応」）</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>\</u></font><font color="blue"><u>61_</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>モニタリングポリシー</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>\</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>★</u></font><font color="blue"><u>BTMUC</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>市場コンプラ管理態勢</u></font><font color="blue"><u>(1.5&amp;2</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>线</u></font><font color="blue"><u>)\</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>★</u></font><font color="blue" face="宋体" style="font-family: 宋体,serif,'EmojiFont';"><u>file_name</u></font><font color="blue"><u>.xlsx</u></font></a></div>
<div align="left" style="text-align: justify;">&nbsp;</div>
<img src="cid:no_phone.PNG" data-custom="AAMkAGU2NWNjZmZjLTNkY2UtNGFiYy1iZjM1LTEzYjMxM2Q1MTFjZgBGAAAAAAAAjMjoipm6R4cG0fcoVRy2BwDwmmOI0ecmRKvtnTObaXr8AAAAAAEMAADwmmOI0ecmRKvtnTObaXr8AACdbEhiAAABEgAQAHsL%2BbbVnmFEjpMdUhKs8T4%3D" naturalheight="601" naturalwidth="459" src="service.svc/s/GetFileAttachment?id=AAMkAGU2NWNjZmZjLTNkY2UtNGFiYy1iZjM1LTEzYjMxM2Q1MTFjZgBGAAAAAAAAjMjoipm6R4cG0fcoVRy2BwDwmmOI0ecmRKvtnTObaXr8AAAAAAEMAADwmmOI0ecmRKvtnTObaXr8AACdbEhiAAABEgAQAHsL%2BbbVnmFEjpMdUhKs8T4%3D&amp;X-OWA-CANARY=DgcknvBpZ0u31fE3APHxKPCNG2-8YtkIu036BMDdmIzO-2okMHUTQmRT7IkcKZoBzJyRhMId15Y.&amp;isImagePreview=True" width="229" height="300" id="x_图片_x0020_1" style="">
<div align="left" style="text-align: justify;"><font face="宋体" size="2" style="font-family: 宋体,serif,'EmojiFont';"><span style="font-size: 10pt;">---------------------------------------------------</span></font></div>
<div align="left" style="text-align: justify;"><font face="宋体" size="2" style="font-family: 宋体,serif,'EmojiFont';"><span style="font-size: 10pt;">-----------------------------------------------------</span></font></div>
<div align="left" style="text-align: justify;">&nbsp;</div>
<div align="left" style="text-align: justify;">&nbsp;</div>
<div align="left" style="text-align: justify;">&nbsp;</div>
</span></font></div>
</div>

    ''',["resource/img/no_phone.PNG"],"wk@cn.test.jp")

def send_Order_cancel():
    from Code.sendMail_exchange import exg_send_mail
    to_address = "hzy@cn.test.jp; fy@cn.test.jp;"
    cc_address = "wk@cn.test.jp;xyy@cn.test.jp"
    subject = "Reminder for Order Cancellation Recording"
    content = "Link: file:fileaddr"
    attachments = ""
    auth = "wk@cn.test.jp"
    exg_send_mail(to_address,cc_address,subject,content,attachments,auth)

def send_all_tran():
    from Code.sendMail_exchange import exg_send_mail
    to_address = "xyy@cn.test.jp"
    cc_address = "tk@cn.test.jp"
    subject = "Volcker Rule EOD: Please submit all today’s derivative and bond transactions!"
    content = "Please submit all today’s derivative and bond transactions!"
    attachments = ""
    auth = "wk@cn.test.jp"
    exg_send_mail(to_address,cc_address,subject,content,attachments,auth)

def send_no_cellphone_notice():
    from Code.sendMail_exchange import exg_send_mail
    to_address = "xyy@cn.test.jp"
    cc_address = "wk@cn.test.jp"
    subject = "No cellphone notice"
    attachments = ["resource/img/no_phone.PNG"]
    content = '''
    <img src="cid:no_phone.PNG" data-custom="AAMkAGU2NWNjZmZjLTNkY2UtNGFiYy1iZjM1LTEzYjMxM2Q1MTFjZgBGAAAAAAAAjMjoipm6R4cG0fcoVRy2BwDwmmOI0ecmRKvtnTObaXr8AAAAAAEMAADwmmOI0ecmRKvtnTObaXr8AACdbEhiAAABEgAQAHsL%2BbbVnmFEjpMdUhKs8T4%3D" naturalheight="601" naturalwidth="459" src="service.svc/s/GetFileAttachment?id=AAMkAGU2NWNjZmZjLTNkY2UtNGFiYy1iZjM1LTEzYjMxM2Q1MTFjZgBGAAAAAAAAjMjoipm6R4cG0fcoVRy2BwDwmmOI0ecmRKvtnTObaXr8AAAAAAEMAADwmmOI0ecmRKvtnTObaXr8AACdbEhiAAABEgAQAHsL%2BbbVnmFEjpMdUhKs8T4%3D&amp;X-OWA-CANARY=DgcknvBpZ0u31fE3APHxKPCNG2-8YtkIu036BMDdmIzO-2okMHUTQmRT7IkcKZoBzJyRhMId15Y.&amp;isImagePreview=True" width="459" height="601" id="x_图片_x0020_1" style="">
    '''
    auth = "wk@cn.test.jp"

    exg_send_mail(to_address,cc_address,subject,content,attachments,auth)