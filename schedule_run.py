import schedule as sc
import time

from main import send_shibor_mail,send_CLO_mail,send_Order_cancel,send_all_tran

sc.every().day.at("10:30").do(send_shibor_mail)
sc.every().day.at("08:30").do(send_CLO_mail)
sc.every().day.at("17:00").do(send_Order_cancel)
sc.every().day.at("17:00").do(send_all_tran)

while True:
    sc.run_pending()
    time.sleep(1)

